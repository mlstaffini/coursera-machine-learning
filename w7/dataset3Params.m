function [C, sigma] = dataset3Params(X, y, Xval, yval)
%DATASET3PARAMS returns your choice of C and sigma for Part 3 of the exercise
%where you select the optimal (C, sigma) learning parameters to use for SVM
%with RBF kernel
%   [C, sigma] = DATASET3PARAMS(X, y, Xval, yval) returns your choice of C and 
%   sigma. You should complete this function to return the optimal C and 
%   sigma based on a cross-validation set.
%

% You need to return the following variables correctly.
C = 1;
sigma = 0.3;

% ====================== YOUR CODE HERE ======================
% Instructions: Fill in this function to return the optimal C and sigma
%               learning parameters found using the cross validation set.
%               You can use svmPredict to predict the labels on the cross
%               validation set. For example, 
%                   predictions = svmPredict(model, Xval);
%               will return the predictions on the cross validation set.
%
%  Note: You can compute the prediction error using 
%        mean(double(predictions ~= yval))
%

minpowr=-2;
maxpowr=1;
range=sort([10.^(minpowr:maxpowr) 10.^(minpowr:maxpowr)*3]);

errorlist=[];
for i=1:length(range)
  for j=1:length(range)
    sigmatemp=range(i);
    Ctemp=range(j);
    
    modeltemp=svmTrain(X, y, Ctemp, @(x1, x2) gaussianKernel(x1, x2, sigmatemp));
    predictionstemp=svmPredict(modeltemp,Xval);
    errortemp=mean(double(predictionstemp ~= yval));
    
    errorlist=[errorlist; [Ctemp sigmatemp errortemp]];
  end
end

[x,ix]=min(errorlist(:,3));


fprintf('C and sigma found in param code.\n');
C=errorlist(ix,1);
sigma=errorlist(ix,2);

% =========================================================================

end
