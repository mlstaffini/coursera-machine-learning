function [J grad] = nnCostFunction(nn_params, ...
                                   input_layer_size, ...
                                   hidden_layer_size, ...
                                   num_labels, ...
                                   X, y, lambda)
%NNCOSTFUNCTION Implements the neural network cost function for a two layer
%neural network which performs classification
%   [J grad] = NNCOSTFUNCTON(nn_params, hidden_layer_size, num_labels, ...
%   X, y, lambda) computes the cost and gradient of the neural network. The
%   parameters for the neural network are "unrolled" into the vector
%   nn_params and need to be converted back into the weight matrices. 
% 
%   The returned parameter grad should be a "unrolled" vector of the
%   partial derivatives of the neural network.
%

% Reshape nn_params back into the parameters Theta1 and Theta2, the weight matrices
% for our 2 layer neural network
Theta1 = reshape(nn_params(1:hidden_layer_size * (input_layer_size + 1)), ...
                 hidden_layer_size, (input_layer_size + 1));

Theta2 = reshape(nn_params((1 + (hidden_layer_size * (input_layer_size + 1))):end), ...
                 num_labels, (hidden_layer_size + 1));

% Setup some useful variables
m = size(X, 1);
         
% You need to return the following variables correctly 
J = 0;
Theta1_grad = zeros(size(Theta1));
Theta2_grad = zeros(size(Theta2));

% ====================== YOUR CODE HERE ======================
% Instructions: You should complete the code by working through the
%               following parts.
% Part 2: Implement the backpropagation algorithm to compute the gradients
%         Theta1_grad and Theta2_grad. You should return the partial derivatives of
%         the cost function with respect to Theta1 and Theta2 in Theta1_grad and
%         Theta2_grad, respectively. After implementing Part 2, you can check
%         that your implementation is correct by running checkNNGradients

%         Hint: We recommend implementing backpropagation using a for-loop
%               over the training examples if you are implementing it for the 
%               first time.
%
% Part 3: Implement regularization with the cost function and gradients.
%
%         Hint: You can implement this around the code for
%               backpropagation. That is, you can compute the gradients for
%               the regularization separately and then add them to Theta1_grad
%               and Theta2_grad from Part 2.
%
% ======================== FEEDFORWARD =======================
% Part 1: Feedforward the neural network and return the cost in the
%         variable J. After implementing Part 1, you can verify that your
%         cost function computation is correct by verifying the cost
%         computed in ex4.m

% Add ones to the X data matrix
X = [ones(m, 1) X];

% Theta1: 25x401
% Theta2: 10x26
% X: 5000x401
% a^(1): 401x1
% a^(2): 26x1 for a single data point
% a^(3): 10x1

a2=sigmoid(Theta1*X'); % 25x5000
a2=[ones(1,size(X,1));a2]; %add bias unit, 26x5000
a3=sigmoid(Theta2*a2); %10x5000

% y: 5000x1, want a matrix of 5000x10 instead with a binary vector for each m

yclasses=(1:num_labels);
Y=(y==yclasses);
%size(Y) % 5000x10

% this a3 is my vector of h_theta(x) for k=1...K, K=10
% J=(1/m)*sum(-Y*log(a3)-(1-Y)*log(1-a3));
% let's unroll cos I'm confused

for i=1:m
  % Y(m,:) is a 1x10
  % a3(:,m) is a 10x1
  Ytemp=Y(i,:);
  a3temp=a3(:,i);
  J=J+(1/m)*(-Ytemp*log(a3temp)-(1-Ytemp)*log(1-a3temp));
end

% ====================== REGULARISED COST ======================

Jreg=(0.5*lambda/m)*(sum(sum(Theta1(:,2:end).^2))+sum(sum(Theta2(:,2:end).^2)));
J=J+Jreg;
% ====================== BACKPROPAGATION ======================

% Theta1: 25x401
% Theta2: 10x26
%Delta1 = zeros(size(Theta1,1),size(Theta1,2)-1); 
%Delta2 = zeros(size(Theta2,1),size(Theta2,2)-1); 
Delta1 = zeros(size(Theta1)); 
Delta2 = zeros(size(Theta2)); 

% being very verbose for clarity
for t=1:m
  % forward prop
  a1m=X(t,:); % 1x401, already got bias unit
  z2m=Theta1*a1m'; %25x1
  a2m=sigmoid(z2m); 
  a2m=[1;a2m]; % 26x1
  z3m=Theta2*a2m; % 10x1
  a3m=sigmoid(z3m);
  % delta for output layer
  delta3m=a3m-Y(t,:)'; % 10x1
  % delta for hidden layer, remember remove bias from delta2m
  % Theta2: 10x26, need Theta2(:,2:end): 10x25
  % delta3m: 10x1
  % sigmoidGradient(z2m): z2m: 25x1
  delta2m=(Theta2(:,2:end)'*delta3m).*sigmoidGradient(z2m);
  Delta2m=delta3m*a2m'; %10x26
  Delta1m=delta2m*a1m; % 25x401
  % accumulate
  Delta1=Delta1+Delta1m;
  Delta2=Delta2+Delta2m;
end

Theta1_grad=(1/m)*Delta1;
Theta2_grad=(1/m)*Delta2;

% ====================== REGULARISE GRAD ======================

Theta1_grad=Theta1_grad+(lambda/m)*[zeros(size(Theta1,1),1) Theta1(:,2:end)];
Theta2_grad=Theta2_grad+(lambda/m)*[zeros(size(Theta2,1),1) Theta2(:,2:end)];

% =========================================================================

% Unroll gradients
grad = [Theta1_grad(:) ; Theta2_grad(:)];

end
